import React from "react";

function Header() {
   return (
      <header class="shadow">
         <nav class="navbar navbar-light bg-light">
            <div class="container-fluid">
               <a class="navbar-brand" href="/">
                  Redux Counter
               </a>
            </div>
         </nav>
      </header>
   );
}

export default Header;
